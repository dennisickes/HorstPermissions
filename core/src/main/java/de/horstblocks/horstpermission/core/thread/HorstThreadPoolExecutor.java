package de.horstblocks.horstpermission.core.thread;

import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

@Getter
public class HorstThreadPoolExecutor extends ThreadPoolExecutor {

    @Setter
    private String name;

    public HorstThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadGroup group) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, new HorstGroupFactory(group, name));
    }

    public HorstThreadPoolExecutor(String name, int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, new HorstGroupFactory(name));
    }
}
