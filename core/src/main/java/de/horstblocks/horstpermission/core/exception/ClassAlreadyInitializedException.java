package de.horstblocks.horstpermission.core.exception;

public class ClassAlreadyInitializedException extends Exception {

    public ClassAlreadyInitializedException(String message) {
        super(message);
    }

    public ClassAlreadyInitializedException(Class clazz) {
        super("The class " + clazz.getName() + " is already initialized");
    }


}
