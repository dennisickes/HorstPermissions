package de.horstblocks.horstpermission.core.database.mysql.result;

import de.horstblocks.horstpermission.core.database.result.Result;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

@Getter
@AllArgsConstructor
public class QueryResult implements Result {

    private final boolean successfully;
    private final List<Throwable> throwables;
    private final PreparedStatement statement;
    private final ResultSet result;

}
