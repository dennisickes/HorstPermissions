package de.horstblocks.horstpermission.core.permissions;

import de.horstblocks.horstpermission.core.database.callback.SingleResultCallback;
import de.horstblocks.horstpermission.core.database.mysql.MySQLDataHandler;
import de.horstblocks.horstpermission.core.database.mysql.result.QueryResult;
import de.horstblocks.horstpermission.core.database.result.VoidResult;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import de.horstblocks.horstpermission.core.permissions.object.PermissionsRank;
import de.horstblocks.horstpermission.core.util.Callback;
import lombok.Getter;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Getter
public abstract class PermissionsDataHandler {

    private static PermissionsDataHandler instance;
    private MySQLDataHandler dataHandler;

    private final HashMap<UUID, List<String>> player_permissions = new HashMap<>();
    private final HashMap<UUID, PermissionsRank> rank_permissions = new HashMap<>();
    private final HashMap<UUID, UUID> player_ranks = new HashMap<>();

    public PermissionsDataHandler(MySQLDataHandler dataHandler) throws ClassAlreadyInitializedException {
        if (instance != null) {
            throw new ClassAlreadyInitializedException(instance.getClass());
        }

        instance = this;
        this.dataHandler = dataHandler;

        getDataHandler().result("SELECT * FROM permissions_ranks", new SingleResultCallback<QueryResult>() {
            @Override
            public void onResult(QueryResult queryResult) {
                if (!queryResult.isSuccessfully()) {
                    System.err.println("Die Gruppen konnte nicht von der Datenbank geladen werden!");
                    return;
                }

                try {
                    while (queryResult.getResult().next()) {
                        UUID id = UUID.fromString(queryResult.getResult().getString("id"));
                        String name = queryResult.getResult().getString("name");
                        PermissionsRank rank = new PermissionsRank(id, name, null, null);

                        getRankPermissions(id, new Callback<List<String>>() {
                            @Override
                            public void onResult(List<String> result) {
                                rank.setPermissions(result);
                                rank_permissions.put(id, rank);
                            }
                        });
                    }
                    System.out.println("Die Gruppen wurden erfolgreich initalisiert!");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } finally {
                    try {
                        if (queryResult.getResult() != null) {
                            queryResult.getResult().close();
                        }

                        if (queryResult.getStatement() != null) {
                            queryResult.getStatement().close();
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void loadPlayerPermissions(UUID player_id, Callback<List<String>> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte kein Datenbank Eintrag durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().result("SELECT * FROM permissions_ranks_player WHERE id='" + player_id.toString() + "'", new SingleResultCallback<QueryResult>() {
            @Override
            public void onResult(QueryResult queryResult) {
                if (!queryResult.isSuccessfully()) {
                    System.err.println("Die Gruppe von " + player_id.toString() + " konnte nicht von der Datenbank geladen werden:");
                    for (Throwable throwable : queryResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(null);
                    return;
                }

                List<String> permissions = new ArrayList<>();
                try {
                    while (queryResult.getResult().next()) {
                        if (queryResult.getResult() != null) {
                            UUID id = UUID.fromString(queryResult.getResult().getString("rank"));

                            if (!rank_permissions.containsKey(id)) {
                                System.err.println("Die Gruppen-ID " + id.toString() + " ist nicht im Cache gespeichert.");
                                callback.onResult(null);
                                return;
                            }
                            player_ranks.put(player_id, id);
                            permissions.addAll(rank_permissions.get(id).getPermissions());
                        }
                    }

                } catch (SQLException ex) {
                    ex.printStackTrace();
                } finally {
                    try {
                        if (queryResult.getResult() == null) {
                            queryResult.getResult().close();
                        }

                        if (queryResult.getStatement() == null) {
                            queryResult.getStatement().close();
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }

                getPermissions(player_id, new Callback<List<String>>() {
                    @Override
                    public void onResult(List<String> result) {
                        if (result == null) {
                            System.err.println("Die Permissions für den Spieler " + player_id.toString() + " konnten nicht geladen werden.");
                            callback.onResult(null);
                            return;
                        }

                        permissions.addAll(result);
                        player_permissions.put(player_id, permissions);
                        callback.onResult(permissions);
                    }
                });
            }
        }).start();
    }

    public void clearPlayerData(UUID id, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte kein Datenbank Eintrag durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("DELETE FROM permissions_player WHERE id='" + id + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Permission vom Spieler " + id.toString() + " konnten nicht gelöscht werden:");

                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }
            }
        }).start();

        getDataHandler().update("DELETE FROM permissions_ranks_player WHERE id='" + id + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Gruppe vom Spieler " + id.toString() + " konnte nicht gelöscht werden:");

                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }
            }
        }).start();
    }

    public void addPermission(UUID id, String permission, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte kein Datenbank Eintrag durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("INSERT INTO permissions_player (id, permission) VALUES ('" + id.toString() + "' , '" + permission + "')", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Permission " + permission + " konnte nicht der UUID{PLAYER} " + id.toString() + " gegeben werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                player_permissions.get(id).add(permission);
                callback.onResult(true);
            }
        }).start();
    }

    public void removePermission(UUID id, String permission, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte kein Datenbank Aktualisierung durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("DELETE FROM permissions_player WHERE id='" + id.toString() + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Permission " + permission + " konnte nicht von der UUID{PLAYER} " + id.toString() + " entfernt werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                player_permissions.get(id).remove(permission);
                callback.onResult(true);
            }
        }).start();
    }

    public void getPermissions(UUID id, Callback<List<String>> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        List<String> permissions = new ArrayList<>();
        getDataHandler().result("SELECT permission FROM permissions_player WHERE id='" + id.toString() + "'", new SingleResultCallback<QueryResult>() {
            @Override
            public void onResult(QueryResult queryResult) {
                if (!queryResult.isSuccessfully()) {
                    System.err.println("Die Permissions von der UUID{PLAYER} " + id.toString() + " konnten nicht von der Datenbank entnommen werden:");
                    for (Throwable throwable : queryResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(null);
                    return;
                }

                try {
                    while (queryResult.getResult().next()) {
                        permissions.add(queryResult.getResult().getString("permission"));
                    }
                    callback.onResult(permissions);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    callback.onResult(null);
                } finally {
                    try {
                        if (queryResult.getResult() != null) {
                            queryResult.getResult().close();
                        }

                        if (queryResult.getStatement() != null) {
                            queryResult.getStatement().close();
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void createRank(String name, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        UUID id = UUID.randomUUID();
        getDataHandler().update("INSERT INTO permissions_ranks(id, name, parent) VALUES ('" + id.toString() + "' , '" + name + "' , 'null')", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Gruppe " + name + " konnte nicht erstellt werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                PermissionsRank rank = new PermissionsRank(id, name, new ArrayList<>(), null);
                rank_permissions.put(id, rank);

                callback.onResult(true);
            }
        }).start();
    }

    public void deleteRank(UUID id, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("DELETE FROM permissions_ranks WHERE id='" + id.toString() + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Gruppe " + id.toString() + " konnte nicht gelöscht werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                rank_permissions.remove(id);

                getDataHandler().update("DELETE FROM permissions_ranks_permissions WHERE id='" + id.toString() + "'", new SingleResultCallback<VoidResult>() {
                    @Override
                    public void onResult(VoidResult voidResult) {
                        if (!voidResult.isSuccessfully()) {
                            System.err.println("Die Gruppe " + id.toString() + " konnte nicht richtig gelöscht werden:");
                            for (Throwable throwable : voidResult.getThrowables()) {
                                throwable.printStackTrace();
                            }

                            callback.onResult(false);
                            return;
                        }

                        callback.onResult(true);
                    }
                });
            }
        }).start();
    }

    public void deleteRank(String name, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("DELETE FROM permissions_ranks WHERE name='" + name + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Gruppe " + name + " konnte nicht gelöscht werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                rank_permissions.remove(getRankIDByName(name));
                getDataHandler().update("DELETE FROM permissions_ranks_permissions WHERE name='" + name + "'", new SingleResultCallback<VoidResult>() {
                    @Override
                    public void onResult(VoidResult voidResult) {
                        if (!voidResult.isSuccessfully()) {
                            System.err.println("Die Gruppe " + name + " konnte nicht richtig gelöscht werden:");
                            for (Throwable throwable : voidResult.getThrowables()) {
                                throwable.printStackTrace();
                            }

                            callback.onResult(false);
                            return;
                        }

                        callback.onResult(true);
                    }
                }).start();
            }
        }).start();
    }

    public void addRankPermission(PermissionsRank rank, String permission, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("INSERT INTO permissions_ranks_permissions (id, name, permission) VALUES ('" + rank.getId().toString() + "' , '" + rank.getName() + "' , '" + permission + "')", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Permission " + permission + " konnte nicht der Gruppe " + rank.getName() + " gegegben werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                rank_permissions.get(rank.getId()).getPermissions().add(permission);
                callback.onResult(true);
            }
        }).start();
    }

    public void removeRankPermission(PermissionsRank rank, String permission, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("DELETE FROM permissions_ranks_permissions WHERE id='" + rank.getId().toString() + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Die Permission " + permission + " konnte nicht von der Gruppe " + rank.getName() + " entfernt werden:");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                rank_permissions.get(rank.getId()).getPermissions().remove(permission);
                callback.onResult(true);
            }
        }).start();
    }

    public void getRankPermissions(UUID id, Callback<List<String>> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().result("SELECT permission FROM permissions_ranks_permissions WHERE id='" + id.toString() + "'", new SingleResultCallback<QueryResult>() {
            @Override
            public void onResult(QueryResult queryResult) {
                if (!queryResult.isSuccessfully()) {
                    System.err.println("Die Permissions von der Gruppe " + id.toString() + " konnten nicht eingelesen werden:");
                    for (Throwable throwable : queryResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(null);
                    return;
                }

                try {
                    List<String> permissions = new ArrayList<>();
                    while (queryResult.getResult().next()) {
                        permissions.add(queryResult.getResult().getString("permission"));
                    }

                    callback.onResult(permissions);
                } catch (SQLException ex) {
                    ex.printStackTrace();
                    callback.onResult(null);
                } finally {
                    try {
                        if (queryResult.getResult() != null) {
                            queryResult.getResult().close();
                        }

                        if (queryResult.getStatement() != null) {
                            queryResult.getStatement().close();
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void hasPlayerRank(UUID player_id, Callback<Integer> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().result("SELECT rank FROM permissions_ranks_player WHERE id='" + player_id.toString() + "'", new SingleResultCallback<QueryResult>() {
            @Override
            public void onResult(QueryResult queryResult) {
                if (!queryResult.isSuccessfully()) {
                    System.out.println("Konnte nicht von der Datenbank herauslesen, ob der Spieler " + player_id.toString() + " einen Rang hat.");
                    for (Throwable throwable : queryResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(-1);
                    return;
                }

                try {
                    while (queryResult.getResult().next()) {
                        if (queryResult.getResult() == null) {
                            callback.onResult(0);
                        } else {
                            callback.onResult(1);
                        }
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } finally {
                    try {
                        if (queryResult.getResult() != null) {
                            queryResult.getResult().close();
                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public void setPlayerRank(UUID player_id, PermissionsRank rank, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        hasPlayerRank(player_id, new Callback<Integer>() {
            @Override
            public void onResult(Integer result) {
                if (result == -1) {
                    System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
                    callback.onResult(false);
                    return;
                }

                if (result == 0) {
                    getDataHandler().update("INSERT INTO permissions_ranks_player (id, rank) VALUES ('" + player_id.toString() + "' , '" + rank.getId().toString() + "')", new SingleResultCallback<VoidResult>() {
                        @Override
                        public void onResult(VoidResult voidResult) {
                            if (!voidResult.isSuccessfully()) {
                                System.err.println("Dem Spieler " + player_id.toString() + " konnte nicht die Gruppe " + rank.getName() + " gesetzt werden.");
                                for (Throwable throwable : voidResult.getThrowables()) {
                                    throwable.printStackTrace();
                                }

                                callback.onResult(false);
                                return;
                            }

                            player_ranks.put(player_id, rank.getId());
                            callback.onResult(true);
                        }
                    }).start();

                    return;
                }

                if (result == 1) {
                    getDataHandler().update("UPDATE permissions_ranks_player SET rank='" + rank.getId().toString() + "' WHERE id='" + player_id.toString() + "';", new SingleResultCallback<VoidResult>() {
                        @Override
                        public void onResult(VoidResult voidResult) {
                            if (!voidResult.isSuccessfully()) {
                                System.err.println("Dem Spieler " + player_id.toString() + " konnte nicht die Gruppe " + rank.getName() + " gesetzt werden.");
                                for (Throwable throwable : voidResult.getThrowables()) {
                                    throwable.printStackTrace();
                                }

                                callback.onResult(false);
                                return;
                            }

                            player_ranks.put(player_id, rank.getId());
                            callback.onResult(true);
                        }
                    }).start();

                    return;
                }
            }
        });
    }

    public void removePlayerRank(UUID player_id, Callback<Boolean> callback) {
        if (!getDataHandler().getMysqlHandler().isConnected()) {
            System.err.println("Es konnte keine Datenbank Aktion durchgeführt werden, da es keine Datenbank Verbindung gibt.");
            return;
        }

        getDataHandler().update("DELETE FROM permissions_ranks_player WHERE id='" + player_id.toString() + "'", new SingleResultCallback<VoidResult>() {
            @Override
            public void onResult(VoidResult voidResult) {
                if (!voidResult.isSuccessfully()) {
                    System.err.println("Dem Spieler " + player_id.toString() + " konnte nicht die Gruppe entfernt werden.");
                    for (Throwable throwable : voidResult.getThrowables()) {
                        throwable.printStackTrace();
                    }

                    callback.onResult(false);
                    return;
                }

                player_ranks.remove(player_id);
                callback.onResult(true);
            }
        }).start();
    }

    public UUID getRankIDByName(String name) {
        for (PermissionsRank rank : rank_permissions.values()) {
            if (rank.getName().equalsIgnoreCase(name)) {
                return rank.getId();
            }
        }

        return null;
    }
}
