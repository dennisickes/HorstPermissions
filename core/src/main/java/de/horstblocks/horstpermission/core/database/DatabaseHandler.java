package de.horstblocks.horstpermission.core.database;

import java.util.concurrent.ThreadPoolExecutor;

public interface DatabaseHandler {

    ThreadPoolExecutor getExecutor();

    void connect();

    void disconnect();
}
