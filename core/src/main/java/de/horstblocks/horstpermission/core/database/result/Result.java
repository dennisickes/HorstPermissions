package de.horstblocks.horstpermission.core.database.result;

import java.util.List;

public interface Result {

    boolean isSuccessfully();

    List<Throwable> getThrowables();
}
