package de.horstblocks.horstpermission.core.permissions.object;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.UUID;

@Getter
@AllArgsConstructor
public class PermissionsRank {

    private final UUID id;
    private String name;

    @Setter
    private List<String> permissions;
    @Setter
    private PermissionsRank parent;

}
