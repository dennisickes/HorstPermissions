package de.horstblocks.horstpermission.core.configuration;

import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.IOException;

@Getter
public class ConfigurationMeta {

    @Setter
    private String name;
    private final File file;
    private final String folderPath;
    private boolean newest = false;

    public ConfigurationMeta(String name, String folderPath) throws IOException {
        this.name = name;
        this.folderPath = folderPath;
        this.file = new File(getFolderPath() + "/" + name + ".json");

        if (!getFile().exists()) {
            if (getFile().createNewFile()) {
                newest = true;
                System.out.println("Configuration " + getName() + ".json successfully created.");
            } else {
                throw new IOException("Could not instantiate configuration " + getName());
            }
        }
    }
}
