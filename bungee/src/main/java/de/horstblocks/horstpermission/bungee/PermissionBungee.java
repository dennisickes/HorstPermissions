package de.horstblocks.horstpermission.bungee;

import de.horstblocks.horstpermission.bungee.command.PermissionsCommand;
import de.horstblocks.horstpermission.bungee.data.DataHandler;
import de.horstblocks.horstpermission.bungee.listener.JoinListener;
import de.horstblocks.horstpermission.core.PermissionService;
import de.horstblocks.horstpermission.core.database.DatabaseHandler;
import de.horstblocks.horstpermission.core.database.mysql.MySQLHandler;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import lombok.Getter;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.IOException;

@Getter
public class PermissionBungee extends Plugin implements PermissionService {

    private static PermissionBungee instance;
    private MySQLHandler mysqlHandler;
    private DataHandler dataHandler;

    @Override
    public void onEnable() {
        instance = this;

        try {
            mysqlHandler = new MySQLHandler();
            getDatabaseHandler().connect();

            dataHandler = new DataHandler(getMysqlHandler().getDataHandler());
        } catch (ClassAlreadyInitializedException | IOException ex) {
            ex.printStackTrace();
        }

        ProxyServer.getInstance().getPluginManager().registerListener(this, new JoinListener(getDataHandler()));
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new PermissionsCommand(getDataHandler()));

        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public DatabaseHandler getDatabaseHandler() {
        return mysqlHandler;
    }

    public static PermissionBungee getInstance() {
        return instance;
    }
}
