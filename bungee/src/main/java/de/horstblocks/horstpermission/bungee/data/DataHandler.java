package de.horstblocks.horstpermission.bungee.data;

import de.horstblocks.horstpermission.core.database.mysql.MySQLDataHandler;
import de.horstblocks.horstpermission.core.exception.ClassAlreadyInitializedException;
import de.horstblocks.horstpermission.core.permissions.PermissionsDataHandler;
import de.horstblocks.horstpermission.core.permissions.object.PermissionsRank;
import de.horstblocks.horstpermission.core.util.Callback;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;
import java.util.UUID;

public class DataHandler extends PermissionsDataHandler {

    public DataHandler(MySQLDataHandler dataHandler) throws ClassAlreadyInitializedException {
        super(dataHandler);
    }

    @Override
    public void addPermission(UUID id, String permission, Callback<Boolean> callback) {
        super.addPermission(id, permission, callback);

        getDataHandler().getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (player.getUniqueId().toString().equals(id.toString())) {
                        player.setPermission(permission, true);
                    }
                }
            }
        }).start();
    }

    @Override
    public void addRankPermission(PermissionsRank rank, String permission, Callback<Boolean> callback) {
        super.addRankPermission(rank, permission, callback);

        getDataHandler().getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    for (UUID id : getPlayer_ranks().keySet()) {
                        if (id.toString().equals(player.getUniqueId().toString())) {
                            UUID rank_id = getPlayer_ranks().get(id);
                            if (rank.getId().toString().equals(rank_id.toString())) {
                                player.setPermission(permission, true);
                            }
                        }
                    }
                }
            }
        }).start();
    }

    @Override
    public void removePermission(UUID id, String permission, Callback<Boolean> callback) {
        super.removePermission(id, permission, callback);

        getDataHandler().getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    if (player.getUniqueId().toString().equals(id.toString())) {
                        player.setPermission(permission, false);
                    }
                }
            }
        }).start();
    }

    @Override
    public void removeRankPermission(PermissionsRank rank, String permission, Callback<Boolean> callback) {
        super.removeRankPermission(rank, permission, callback);

        getDataHandler().getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    for (UUID id : getPlayer_ranks().keySet()) {
                        if (id.toString().equals(player.getUniqueId().toString())) {
                            UUID rank_id = getPlayer_ranks().get(id);
                            if (rank.getId().toString().equals(rank_id.toString())) {
                                player.setPermission(permission, false);
                            }
                        }
                    }
                }
            }
        }).start();
    }

    @Override
    public void deleteRank(UUID id, Callback<Boolean> callback) {
        Thread thread = getDataHandler().getMysqlHandler().getExecutor().getThreadFactory().newThread(new Runnable() {
            @Override
            public void run() {
                for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
                    for (UUID player_id : getPlayer_ranks().keySet()) {
                        if (player_id.toString().equals(player.getUniqueId().toString())) {
                            UUID rank_id = getPlayer_ranks().get(player_id);
                            if (rank_id.toString().equals(id.toString())) {
                                getRank_permissions().get(rank_id).getPermissions().forEach(permission -> {
                                    player.setPermission(permission, false);
                                });

                            }
                        }
                    }
                }
            }
        });
        thread.start();

        try {
            thread.join();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }

        super.deleteRank(id, callback);
    }

    @Override
    public void deleteRank(String name, Callback<Boolean> callback) {
        UUID id = getRankIDByName(name);
        if (id == null) {
            return;
        }

        deleteRank(id, callback);
    }

    @Override
    public void clearPlayerData(UUID id, Callback<Boolean> callback) {
        super.clearPlayerData(id, callback);

        for (ProxiedPlayer player : ProxyServer.getInstance().getPlayers()) {
            if (player.getUniqueId().toString().equals(id.toString())) {
                player.getPermissions().clear();
            }
        }
    }
}
