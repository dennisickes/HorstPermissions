package de.horstblocks.horstpermission.bungee.listener;

import de.horstblocks.horstpermission.bungee.data.DataHandler;
import de.horstblocks.horstpermission.core.util.Callback;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.List;

@Getter
@AllArgsConstructor
public class JoinListener implements Listener {

    private final DataHandler dataHandler;

    @EventHandler
    public void onJoin(ServerConnectedEvent e) {
        ProxiedPlayer player = e.getPlayer();

        if (getDataHandler().getPlayer_permissions().containsKey(player.getUniqueId())) {
            return;
        }

        getDataHandler().loadPlayerPermissions(player.getUniqueId(), new Callback<List<String>>() {
            @Override
            public void onResult(List<String> result) {
                for (String permission : result) {
                    player.setPermission(permission, true);
                }
            }
        });
    }

}
