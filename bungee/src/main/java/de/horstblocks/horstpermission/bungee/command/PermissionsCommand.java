package de.horstblocks.horstpermission.bungee.command;

import de.horstblocks.horstpermission.bungee.data.DataHandler;
import de.horstblocks.horstpermission.core.PermissionService;
import de.horstblocks.horstpermission.core.permissions.object.PermissionsRank;
import de.horstblocks.horstpermission.core.util.Callback;
import de.horstblocks.horstpermission.core.util.UUIDFetcher;
import lombok.Getter;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

@Getter
public class PermissionsCommand extends Command {

    private final DataHandler dataHandler;

    public PermissionsCommand(DataHandler dataHandler) {
        super("permissions", "", "permission", "per");

        this.dataHandler = dataHandler;
    }

    @Override
    public void execute(CommandSender cs, String[] args) {
        if (!(cs instanceof ProxiedPlayer)) {

        }

        ProxiedPlayer player = (ProxiedPlayer) cs;

        if (!player.hasPermission("horstblocks.horstpermission")) {
            player.sendMessage(PermissionService.getPrefix() + "§cDu hast nicht die Berechtigung diesen Befehl auszuführen!");
            return;
        }

        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("group")) {
                if (args[1].equalsIgnoreCase("list")) {
                    player.sendMessage("§aGroups:");
                    for (PermissionsRank rank : getDataHandler().getRank_permissions().values()) {
                        player.sendMessage("§8- §c" + rank.getName() + " {" + rank.getId().toString() + "}");
                    }

                    return;
                }

                String name = args[1];
                if (getDataHandler().getRankIDByName(name) == null) {
                    player.sendMessage(PermissionService.getPrefix() + "Diese Gruppe ist nicht in der Datenbank registriert.");
                    return;
                }
                PermissionsRank rank = getDataHandler().getRank_permissions().get(getDataHandler().getRankIDByName(name));

                player.sendMessage("§a" + rank.getName() + " §c{" + rank.getId().toString() + "}");
                if (rank.getParent() != null) player.sendMessage("§cParent: §a" + rank.getParent().getName() + " §c{" + rank.getParent().getId().toString() + "}");
                player.sendMessage("§cPermissions:");
                for (String permission : rank.getPermissions()) {
                    player.sendMessage(" §8- §f" + permission);
                }

                return;
            }

            if (args[0].equalsIgnoreCase("user")) {
                String name = args[1];
                UUID id = UUIDFetcher.getUUID(name);

                if (id == null) {
                    player.sendMessage(PermissionService.getPrefix() + "§cDer Spieler konnte nicht gefunden werden!");
                    return;
                }

                player.sendMessage("§a" + name + " §c{" + id.toString() + "}");

                PermissionsRank rank = null;
                if (getDataHandler().getPlayer_ranks().containsKey(id)) {
                    rank = getDataHandler().getRank_permissions().get(getDataHandler().getPlayer_ranks().get(id));
                }

                if (rank == null) {
                    player.sendMessage(" §aGruppe: §cdefault");
                } else {
                    player.sendMessage(" §aGruppe: §c" + rank.getName());
                }
                player.sendMessage(" §cPermissions:");
                for (String permission : getDataHandler().getPlayer_permissions().get(id)) {
                    player.sendMessage( "§8- §f" + permission);
                }

                return;
            }

            return;
        }

        if (args.length == 3) {
            if (args[0].equalsIgnoreCase("group")) {
                String name = args[1];
                if (args[2].equalsIgnoreCase("create")) {

                    if (name.length() > 16) {
                        player.sendMessage(PermissionService.getPrefix() + "§cDer Gruppenname darf maximal nur 16 Zeichen besitzen.");
                        return;
                    }

                    if (getDataHandler().getRankIDByName(name) != null) {
                        player.sendMessage(PermissionService.getPrefix() + "§cDer Gruppenname ist schon besetzt.");
                        return;
                    }

                    getDataHandler().createRank(name, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Gruppe konnte nicht erstellt werden, bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Gruppe wurde erfolgreich erstellt.");
                        }
                    });

                    return;
                }

                if (args[2].equalsIgnoreCase("delete")) {
                    if (getDataHandler().getRankIDByName(name) == null) {
                        player.sendMessage(PermissionService.getPrefix() + "§cDiese Gruppe ist nicht in der Datenbank registriert.");
                        return;
                    }

                    getDataHandler().deleteRank(name, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Gruppe konnte nicht gelöscht werden, bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Gruppe wurde erfolgreich gelöscht.");
                        }
                    });

                    return;
                }

                return;
            }

            if (args[0].equalsIgnoreCase("user")) {
                String name = args[1];
                UUID id = UUIDFetcher.getUUID(name);

                if (id == null) {
                    player.sendMessage("Der Spieler konnte nicht gefunden werden!");
                    return;
                }

                if (args[2].equalsIgnoreCase("clear")) {
                    getDataHandler().clearPlayerData(id, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Spielerdaten von " + name + " konnten nicht gelöscht werden. Bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Spielerdaten von " + name + " wurden erfolgreich gelöscht!");
                        }
                    });

                    return;
                }

                return;
            }

            return;
        }

        if (args.length == 4) {
            if (args[0].equalsIgnoreCase("group")) {
                String name = args[1];

                if (getDataHandler().getRankIDByName(name) == null) {
                    player.sendMessage(PermissionService.getPrefix() + "Diese Gruppe ist nicht in der Datenbank registriert.");
                    return;
                }
                PermissionsRank rank = getDataHandler().getRank_permissions().get(getDataHandler().getRankIDByName(name));

                if (args[2].equalsIgnoreCase("add")) {
                    String permission = args[3];

                    getDataHandler().addRankPermission(rank, permission, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Permission konnte der Gruppe nicht zugewiesen werden, bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Permission wurde erfolgreich der Gruppe zugewiesen.");
                        }
                    });

                    return;
                }

                if (args[2].equalsIgnoreCase("remove")) {
                    String permission = args[3];

                    getDataHandler().removeRankPermission(rank, permission, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Permission konnte von der Gruppe nicht entfernt werden, bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Permission wurde erfolgreich von der Gruppe entfernt.");
                        }
                    });

                    return;
                }

                return;
            }

            if (args[0].equalsIgnoreCase("user")) {
                String name = args[1];
                UUID id = UUIDFetcher.getUUID(name);

                if (id == null) {
                    player.sendMessage("Der Spieler konnte nicht gefunden werden!");
                    return;
                }

                if (args[2].equalsIgnoreCase("add")) {
                    String permission = args[3];

                    getDataHandler().addPermission(id, permission, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Permission konnte dem Spieler nicht zugewiesen werden, bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Permission wurde erfolgreich dem Spieler zugewiesen.");
                        }
                    });

                    return;
                }

                if (args[2].equalsIgnoreCase("remove")) {
                    String permission = args[3];

                    getDataHandler().removePermission(id, permission, new Callback<Boolean>() {
                        @Override
                        public void onResult(Boolean result) {
                            if (!result) {
                                player.sendMessage(PermissionService.getPrefix() + "§cDie Permission konnte dem Spieler nicht entfernt werden, bitte benachrichtige diesen Fehler einem Entwickler.");
                                return;
                            }

                            player.sendMessage(PermissionService.getPrefix() + "§aDie Permission wurde erfolgreich von dem Spieler entfernt.");
                        }
                    });

                    return;
                }

                return;
            }
        }

    }
}
